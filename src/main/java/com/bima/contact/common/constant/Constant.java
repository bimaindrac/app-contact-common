package com.bima.contact.common.constant;

public class Constant {

	private Constant() {
	}
	
	public static final String SUCCESS_RESPONSE_CODE = "0";
	public static final String SUCCESS_RESPONSE_DESC = "Success";
	
}
