package com.bima.contact.common.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponseDto<T> implements Serializable {
	
	private static final long serialVersionUID = -3838662043381150516L;
	
	private String responseCode;
	private String responseDesc;
	private transient T data;
	
}
