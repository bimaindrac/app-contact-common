package com.bima.contact.common.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BaseListResponseDto<T> implements Serializable {
	
	private static final long serialVersionUID = -6461699042627598575L;
	
	private String responseCode;
	private String responseDesc;
	private transient List<T> data;
	
}
